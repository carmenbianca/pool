// TODO: Make magic numbers global variables.
// TODO: Player instructions.
// TODO: Implement game rules.
// TODO: Create starting position.

var scene;
var camera;
var clock;
var container;
var renderer;

var balls;
var holes;
var direction;
var line;
var power_modifier;
var state;
var balls_out;
var shooting_ball;
var another_turn;
var player;
var collided_balls;
var solid_player;
var striped_player;

var WHITE_BALL = 1;
var EIGHT_BALL = 2;
var SOLID_BALL = 3;
var STRIPED_BALL = 4;

var STATE_STATIC = 1;
var STATE_ROLLING = 2;

var BALL_Y = 1.5;

var TABLE_DEPTH = 50;
var TABLE_WIDTH = 25;

var ASCII = {}
for (char of ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "A", "S", "D",
              "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N",
              "M"]) {
    ASCII[char.charCodeAt(0)] = char;
}

function render() {
    //camera.lookAt(scene.position);

    renderer.render(scene, camera);
}

function animate() {
    requestAnimationFrame(animate);
    var delta_time = clock.getDelta();

    if (state == STATE_ROLLING) {
        for (ball of balls) {
            ball.update(delta_time);
        }
        for (ball of balls) {
            do_collisions_for_ball(ball, balls, delta_time);
        }

        // Maybe put this in a function. Here for now is OK.
        for (ball of balls) {
            for (hole of holes) {
                if (ball.is_aabb_overlap_with(hole)) {
                    if (ball.is_collide_with(hole)) {
                        // handle ball fall into hole.
                        if (ball.ball_type == EIGHT_BALL)
                        {
                            check_win_conditions(ball);
                        }
                        else if (ball.ball_type == WHITE_BALL)
                        {
                            temp_remove_white_ball(ball);
                        }
                        else{
                            remove_ball_from_field(ball);
                        }
                    }
                }
            }
        }

        var rolling = any_balls_rolling(balls);
        // Intermediary state
        if (!rolling) {
            // These things all need to be executed once in the transition to
            // STATE_STATIC.  Consider this if-statement a transitional state.
            if (!check_for_ball_type(WHITE_BALL))
            {
                shooting_ball.position.z = 15;
                balls.push(shooting_ball);
                another_turn = false;
            }

            if (another_turn == false)
            {
                player = player % 2 + 1;
                //console.log(""+player);
                document.getElementById("player").innerHTML = ""+player;
            }
            another_turn = false;

            state = STATE_STATIC;
            replace_old_line();
        }
    }

    //console.log("" + balls[0].velocity.x + " " + balls[0].velocity.z);

    render();
}

function resize() {
    var factor = 1;
    var width = window.innerWidth * factor;
    var height = window.innerHeight * factor;
    renderer.setSize(width, height);

    camera.aspect = width / height;
    camera.updateProjectionMatrix();
}

function onDocumentKeyDown(event) {
    var keyCode = event.which;

    if (state == STATE_STATIC) {
        if (ASCII[keyCode] == "F") {
            shooting_ball.velocity.set(direction.x * power_modifier,
                                  0,
                                  direction.z * power_modifier);
            line.visible = false;
            state = STATE_ROLLING;
        }
        else if (ASCII[keyCode] == "A") {
            var angle = Math.PI / 128;
            direction = rotate_direction(direction, angle);
            replace_old_line();
        }
        else if (ASCII[keyCode] == "D") {
            var angle = -Math.PI / 128;
            direction = rotate_direction(direction, angle);
            replace_old_line();
        }
        if (power_modifier < 100) {
            if (ASCII[keyCode] == "W") {
                power_modifier += 1;
                document.getElementById("power").innerHTML = ""+power_modifier;
            }
        }
        if (power_modifier > 1) {
            if (ASCII[keyCode] == "S") {
                power_modifier -= 1;
                document.getElementById("power").innerHTML = ""+power_modifier;
            }
        }
    }
}

class PoolTable extends THREE.Group {

    constructor() {
        super();

        var loader = new THREE.TextureLoader();
        var surfaceTexture = loader.load("resources/cloth.jpg");
        var surfaceMaterial = new THREE.MeshLambertMaterial({map: surfaceTexture});
        var wallTexture = loader.load("resources/crate.jpg");
        var wallMaterial = new THREE.MeshLambertMaterial({map: wallTexture});

        var surfaceGeometry = new THREE.BoxGeometry(TABLE_WIDTH, 1, TABLE_DEPTH);
        var surfaceMesh = new THREE.Mesh(surfaceGeometry, surfaceMaterial);

        var bottomGeometry = new THREE.BoxGeometry(
            surfaceGeometry.parameters.width,
            0.01,
            surfaceGeometry.parameters.depth
        );
        var bottomMesh = new THREE.Mesh(bottomGeometry, wallMaterial);
        bottomMesh.position.set(0, -surfaceGeometry.parameters.height / 2, 0);

        var longSideGeometry = new THREE.BoxGeometry(
            1, 2, surfaceGeometry.parameters.depth);
        var longSideMeshLeft = new THREE.Mesh(longSideGeometry, wallMaterial);
        longSideMeshLeft.position.set(
            -surfaceGeometry.parameters.width / 2 - longSideGeometry.parameters.width / 2,
            surfaceGeometry.parameters.height / 2,
            0);
        var longSideMeshRight = longSideMeshLeft.clone();
        longSideMeshRight.position.set(
            surfaceGeometry.parameters.width / 2 + longSideGeometry.parameters.width / 2,
            surfaceGeometry.parameters.height / 2,
            0);

        var shortSideGeometry = new THREE.BoxGeometry(
            surfaceGeometry.parameters.width + longSideGeometry.parameters.width * 2, 2, 1);
        var shortSideMeshFront = new THREE.Mesh(shortSideGeometry, wallMaterial);
        shortSideMeshFront.position.set(
            0,
            surfaceGeometry.parameters.height / 2,
            -surfaceGeometry.parameters.depth / 2 - shortSideGeometry.parameters.depth / 2);
        var shortSideMeshBack = shortSideMeshFront.clone();
        shortSideMeshBack.position.set(
            0,
            surfaceGeometry.parameters.height / 2,
            surfaceGeometry.parameters.depth / 2 + shortSideGeometry.parameters.depth / 2);

        this.add(surfaceMesh);
        this.add(bottomMesh);
        this.add(longSideMeshLeft);
        this.add(longSideMeshRight);
        this.add(shortSideMeshFront);
        this.add(shortSideMeshBack);
    }
}

class Hole extends THREE.Group {

    constructor() {
        super();
        this.geometry = new THREE.CylinderGeometry(1.5, 1.5, 1);
        // This is a lie. Makes board prettier and causes balls to not fall in
        // immediately upon touching.
        this.radius = (this.geometry.parameters.radiusTop / 2) + 0.25;
        this.material = new THREE.MeshLambertMaterial({color: 0x000000});
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.add(this.mesh);
    }
}

class Ball extends THREE.Group {

    constructor(ball_type=-1) {
        super();

        this.ball_type = ball_type;

        this.velocity = new THREE.Vector3(0, 0, 0);

        this.geometry = new THREE.SphereGeometry(1);
        this.radius = this.geometry.parameters.radius;

        if (this.ball_type == EIGHT_BALL) {
            var material = this._make_material("resources/8ball.png");
        }
        else if (this.ball_type == WHITE_BALL) {
            var material = new THREE.MeshPhongMaterial({color: 0xffffff});
        }
        else if (this.ball_type == SOLID_BALL) {
            var material = new THREE.MeshPhongMaterial({color:0xff0000});
        }
        else if (this.ball_type == STRIPED_BALL) {
            var material = new THREE.MeshPhongMaterial({color:0x0000ff});
        }
        else {
            console.log("This is not supposed to happen. Maybe throw an error here?");
        }

        this.mesh = new THREE.Mesh(this.geometry, material);
        this.add(this.mesh);
    }

    update(delta_time) {
        // Reduce velocity (friction)
        this.velocity.multiplyScalar(1 - 0.25 * delta_time);

        // Stop ball is below velocity threshold.
        if (this.velocity.length() < 0.75) {
            this.velocity.set(0, 0, 0);
        }

        // Check if ball out of bounds. Put it back in bounds and reverse
        // velocity. Would be neat to do this programmatically rather than
        // repeating four times, but this will have to suffice for now.
        // Maybe reduce velocity when hitting wall?
        if (this.position.z >= TABLE_DEPTH / 2 - this.radius) {
            this.position.z -= (this.position.z - (TABLE_DEPTH / 2 - this.radius)) * 2;
            this.velocity.z = -this.velocity.z;
        }
        if (this.position.z <= -TABLE_DEPTH / 2 + this.radius) {
            this.position.z -= (this.position.z + (TABLE_DEPTH / 2 - this.radius)) * 2;
            this.velocity.z = -this.velocity.z;
        }
        if (this.position.x >= TABLE_WIDTH / 2 - this.radius) {
            this.position.x -= (this.position.x - (TABLE_WIDTH / 2 - this.radius)) * 2;
            this.velocity.x = -this.velocity.x;
        }
        if (this.position.x <= -TABLE_WIDTH / 2 + this.radius) {
            this.position.x -= (this.position.x + (TABLE_WIDTH / 2 - this.radius)) * 2;
            this.velocity.x = -this.velocity.x;
        }

        // Disallow any velocity in y direction.  Could use 2D vector, but then
        // velocity.y determines position.z, which is just confusing.
        // Technically this is superfluous to do at every update, but better to
        // be safe than sorry.
        this.velocity.y = 0;

        // Actually move the ball.
        this.position.set(this.position.x + (this.velocity.x * delta_time),
                          BALL_Y,
                          this.position.z + (this.velocity.z * delta_time));
    }

    is_aabb_overlap_with(other) {
        return (this.position.x + this.radius + other.radius > other.position.x
                && this.position.x < other.position.x + this.radius + other.radius
                && this.position.z + this.radius + other.radius > other.position.z
                && this.position.z < other.position.z + this.radius + other.radius);
    }

    is_collide_with(other) {
        var distance = Math.sqrt(
            Math.pow(this.position.x - other.position.x, 2) + Math.pow(this.position.z - other.position.z, 2)
        );
        return distance < this.radius + other.radius;
    }

    collision_point_with(other) {
        var collision_point = new THREE.Vector3(0, 0, 0);
        collision_point.x = ((this.position.x * other.radius) + (other.position.x * this.radius))
            / (this.radius + other.radius);
        collision_point.z = ((this.position.z * other.radius) + (other.position.z * this.radius))
            / (this.radius + other.radius);
        return collision_point;
    }

    _make_material(texture_path) {
        var loader = new THREE.TextureLoader();
        var texture = loader.load(texture_path);
        var surfaceMaterial = new THREE.MeshPhongMaterial({map: texture});
        return surfaceMaterial;
    }
}

function temp_remove_white_ball (ball) {
    //remove the white ball
    var i = balls.indexOf(ball);
    balls.splice(i, 1);
    ball.position.z = 30;
    ball.position.x = 0;
    ball.velocity.x = 0;
    ball.velocity.z = 0;
}

function check_for_ball_type(type) {
    for (ball of balls) {
        if (ball.ball_type == type) {
            return true;
        }
    }
    return false;
}

function check_win_conditions(ball) {
    //remove the 8-ball and check the win conditions
    var message = "";
    var i = balls.indexOf(ball);
    balls.splice(i, 1);
    ball.position.z = -30;
    ball.position.x = 0;
    //striped = player 1
    if (player == striped_player)
    {
        //handle
        if (!check_for_ball_type(STRIPED_BALL))
        {
            message = (striped_player == 1 ? "player 1 wins!!!" : "player 2 wins!!!");
        }
        else
        {
            message = (striped_player == 1 ? "player 2 wins!!!" : "player 1 wins!!!");
        }
    }
    //solid = player 2
    if (player == solid_player)
    {
        //handle
        if (!check_for_ball_type(SOLID_BALL))
        {
            message = (solid_player == 1 ? "player 1 wins!!!" : "player 2 wins!!!");
        }
        else
        {
            message = (solid_player == 1 ? "player 2 wins!!!" : "player 1 wins!!!");
        }
    }
    else {
        message = (player == 1 ? "player 2 wins!!!" : "player 1 wins!!!");
    }
    document.getElementById("win").innerHTML = ""+message;
}

function remove_ball_from_field (ball) {
    //handle removal of ball
    var i = balls.indexOf(ball);
    balls.splice(i, 1);
    if (ball.ball_type == STRIPED_BALL) {
        ball.position.z = 25 - balls_out[0].length * 2;
        balls_out[0].push(ball);
        ball.position.x = 15;
        if (balls_out[0].length + balls_out[1].length == 1) {
            striped_player = player;
            solid_player = player % 2 + 1;
            document.getElementById("playerOneColour").innerHTML = ((player == 1) ? "Blue" : "Red");
            document.getElementById("playerTwoColour").innerHTML = ((player == 1) ? "Red" : "Blue");
        }
        if (player == striped_player)
        {
            another_turn = true;
        }
    }
    if (ball.ball_type == SOLID_BALL) {
        ball.position.z = 25 - balls_out[1].length * 2;
        balls_out[1].push(ball);
        ball.position.x = -15;
        if (balls_out[0].length + balls_out[1].length == 1) {
            solid_player = player;
            striped_player = player % 2 + 1;
            document.getElementById("playerOneColour").innerHTML = ((player == 1) ? "Red" : "Blue");
            document.getElementById("playerTwoColour").innerHTML = ((player == 1) ? "Blue" : "Red");
        }
        if (player == solid_player)
        {
            another_turn = true;
        }
    }
}

function balls_in_collision(ball, other) {
    for (pair of collided_balls) {
        if ((ball == pair[0] && other == pair[1]) || (ball == pair[1] && other == pair[0])) {
            return true;
        }
    }
    return false;
}

//explicit near copy of balls_in_collision because the splice function needs the i of the for loop.
function splice_balls(ball, other) {
    for (i = 0; i < collided_balls.length; i++) {
        pair = collided_balls[i];
        if ((ball == pair[0] && other == pair[1]) || (ball == pair[1] && other == pair[0])) {
            collided_balls.splice(i,1);
        }
    }
}

function do_collisions_for_ball(ball, balls, delta_time) {
    for (other of balls) {
        if (ball == other) {
            continue;
        }
        if (ball.is_aabb_overlap_with(other)) {
            if (ball.is_collide_with(other)) {
                if (!balls_in_collision(ball, other)) {
                    var collision_point = ball.collision_point_with(other);
                    var new_velocities = calculate_new_velocities(ball, other);

                    var velocity_ball = new_velocities[0];
                    ball.velocity.x = velocity_ball.x;
                    ball.velocity.z = velocity_ball.z;

                    var velocity_other = new_velocities[1];
                    other.velocity.x = velocity_other.x;
                    other.velocity.z = velocity_other.z;

                    ball.position.x = ball.position.x + velocity_ball.x * delta_time;
                    ball.position.z = ball.position.z + velocity_ball.z * delta_time;
                    other.position.x = other.position.x + velocity_other.x * delta_time;
                    other.position.z = other.position.z + velocity_other.z * delta_time;
                    collided_balls.push([ball, other]);
                    //console.log(""+collided_balls);
                }

            }
            else {
                splice_balls(ball, other);
            }
        }
        else {
            splice_balls(ball, other);
        }
    }
}

function calculate_new_velocities(first, second) {
    var velocity_first = new THREE.Vector3(0, 0, 0);
    var velocity_second = new THREE.Vector3(0, 0, 0);

    var impact_angle = angle_between_coordinates(first.position, second.position);
    var velocity_angle_first = angle_of_vector(first.velocity);
    var velocity_angle_second = angle_of_vector(second.velocity);

    var angle_subtraction_first = velocity_angle_first - impact_angle;
    var angle_subtraction_second = velocity_angle_second - impact_angle;

    //velocity_first.x = second.velocity.x;
    velocity_first.x = second.velocity.length() * Math.cos(angle_subtraction_second)
        * Math.cos(impact_angle)
        + first.velocity.length() * Math.sin(angle_subtraction_first) * Math.cos(impact_angle + Math.PI / 2);

    // velocity_first.z = second.velocity.z;
    velocity_first.z = second.velocity.length() * Math.cos(angle_subtraction_second)
        * Math.sin(impact_angle)
        + first.velocity.length() * Math.sin(angle_subtraction_first) * Math.sin(impact_angle + Math.PI / 2);

    // velocity_second.x = first.velocity.x;
    velocity_second.x = first.velocity.length() * Math.cos(angle_subtraction_first)
        * Math.cos(impact_angle)
        + second.velocity.length() * Math.sin(angle_subtraction_second) * Math.cos(impact_angle + Math.PI / 2);

    // velocity_second.z = first.velocity.z;
    velocity_second.z = first.velocity.length() * Math.cos(angle_subtraction_first)
        * Math.sin(impact_angle)
        + second.velocity.length() * Math.sin(angle_subtraction_second) * Math.sin(impact_angle + Math.PI / 2);

    return [velocity_first, velocity_second];
}

function angle_of_vector(vector) {
    return Math.atan2(vector.z, vector.x);
}

function angle_between_coordinates(first, second) {
    /* var first_vector = new THREE.Vector2(first.x, first.z);
    first_vector.normalize();
    var second_vector = new THREE.Vector2(first.x, first.z);
    second_vector.normalize();
    var dot_product = first_vector.dot(second_vector);
    //console.log(dot_product);
    var result = Math.acos(dot_product);
    //console.log(result); */
    var deltaX = second.x - first.x;
    var deltaZ = second.z - first.z;
    result = Math.atan2(deltaZ, deltaX)
    return result;
}

function draw_aim_line(ball, direction) {
    var geometry = new THREE.Geometry();
    var material = new THREE.LineBasicMaterial({color: 0xffffff});
    geometry.vertices.push(ball.position,
                           new THREE.Vector3(ball.position.x + direction.x*5,
                                             BALL_Y,
                                             ball.position.z + direction.z*5));
    return new THREE.Line(geometry, material);
}

// Function uses global variables. Beware.
function replace_old_line() {
    line.geometry.dispose();
    line.material.dispose();
    scene.remove(line);
    line = draw_aim_line(shooting_ball, direction);
    scene.add(line);
}

function rotate_direction(direction, angle) {
    var new_direction = new THREE.Vector3(direction.x, direction.y, direction.z);
    var axis = new THREE.Vector3(0, 1, 0);
    new_direction.applyAxisAngle(axis, angle);
    return new_direction;
}

function any_balls_rolling(balls) {
    for (ball of balls) {
        if (ball.velocity.length() > 0) {
            return true;
        }
    }
    return false;
}

function init() {

    // DOCUMENT

    container = document.createElement("div");
    document.body.appendChild(container);

    // SCENE

    scene = new THREE.Scene();

    // CAMERA

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 2000);
    camera.position.y = 20;
    camera.position.x = -30;
    camera.position.z = -20;

    scene.add(camera);

    // LIGHTS

    var ambientLight = new THREE.AmbientLight(0x404040, 1.0);
    scene.add(ambientLight);

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 1.0 );
    directionalLight.position.set( 2000, 1250, 100 );
    scene.add( directionalLight );

    // MESH STUFF

    var poolTable = new PoolTable();
    scene.add(poolTable);

    function rand_ord() {
        return (Math.round(Math.random())-0.5);
    }

    positions = [[0,-11],[-1,-13],[1,-13],[-2,-15],[2,-15],[-3,-17],[-1,-17],[1,-17],[3,-17],[-4,-19],[-2,-19],[0,-19],[2,-19],[4,-19]];
    positions.sort(rand_ord);


    balls = [];
    shooting_ball = new Ball(ball_type=WHITE_BALL);
    balls.push(shooting_ball);
    shooting_ball.position.set(0, BALL_Y, 15)
    var eight_ball = new Ball(ball_type=EIGHT_BALL);
    balls.push(eight_ball);
    eight_ball.position.set(0, BALL_Y, -15)
    eight_ball.rotateZ(Math.PI/2);
    eight_ball.rotateX(Math.PI/2);
    for (var i = 0; i < 7; i++) {
        pos1 = positions[i];
        pos2 = positions[13-i]

        var solid_ball = new Ball(ball_type=SOLID_BALL);
        balls.push(solid_ball);
        //console.log(""+pos1[0]+" "+pos1[1]);
        //console.log(""+pos2[0]+" "+pos2[1]);
        solid_ball.position.set(pos1[0], BALL_Y, pos1[1]);

        var striped_ball = new Ball(ball_type=STRIPED_BALL);
        balls.push(striped_ball);
        striped_ball.position.set(pos2[0], BALL_Y, pos2[1]);
    }

    for (ball of balls) {
        scene.add(ball);
    }

    holes = []
    for (var i = 0; i < 6; i++) {
        holes.push(new Hole());
        scene.add(holes[i]);
    }

    holes[0].position.set((TABLE_WIDTH - holes[0].radius) / 2, 0.1, 0);
    holes[1].position.set((-TABLE_WIDTH + holes[1].radius) / 2, 0.1, 0);
    holes[2].position.set((TABLE_WIDTH - holes[2].radius) / 2, 0.1,
                          (TABLE_DEPTH - holes[2].radius) / 2);
    holes[3].position.set((-TABLE_WIDTH + holes[3].radius) / 2, 0.1,
                          (TABLE_DEPTH - holes[3].radius) / 2);
    holes[4].position.set((TABLE_WIDTH - holes[4].radius) / 2, 0.1,
                          (-TABLE_DEPTH + holes[4].radius) / 2);
    holes[5].position.set((-TABLE_WIDTH + holes[5].radius) / 2, 0.1,
                          (-TABLE_DEPTH + holes[5].radius) / 2);

    var loader = new THREE.TextureLoader();
    var skyGeo = new THREE.SphereGeometry(1000, 60, 40);
    var skyTexture = loader.load("resources/SkyDome.jpg");
    var skyMat = new THREE.MeshBasicMaterial({map: skyTexture, side: THREE.BackSide});
    var skyMesh = new THREE.Mesh(skyGeo, skyMat);
    //skyMesh.scale.set(-1, 1, 1);
    //skyMesh.eulerOrder = 'XZY';
    skyMesh.renderDepth = 1000.0;
    scene.add(skyMesh);

    // RENDERER

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    // CONTROLS

    var controls = new THREE.OrbitControls(camera, renderer.domElement);
    //controls.enableZoom = false;

    // CLOCK

    clock = new THREE.Clock();

    // ALL OTHER THREEJS STUFF THAT NEEDS VARIABLES

    direction = new THREE.Vector3(0, 0, -1);
    // This line needs to be removed from scene when balls are rolling, and
    // re-added when balls are not rolling.
    line = draw_aim_line(shooting_ball, direction);
    scene.add(line);

    power_modifier = 20;

    state = STATE_STATIC;

    balls_out = [
        [],
        []
    ];

    collided_balls = [];

    player = 1;
    another_turn = false;

    // LISTENERS

    document.addEventListener("keydown", onDocumentKeyDown);
    window.addEventListener("resize", resize, false);
}

init();
animate();
